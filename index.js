const express = require('express')
const bodyParser = require('body-parser');
const { check, validationResult } = require('express-validator')
const moment = require('moment')
const dbConnect = require('./db')
const { google } = require('googleapis')
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const app = express();
const gcal = require('./gcalendar')
const Bike = require('./models/Bike')
const Customer = require('./models/Customer')
const fs = require('fs')
const constants = require('./constants')
const axios = require('axios')
const urls = require('./config.json')







app.post('/dialogflow', express.json(), (req, res) => {
  const agent = new WebhookClient({ request: req, response: res });
  const session_id = agent.session.split("/").slice(-1)[0];
  // console.log(agent.session.split("/").slice(-1)[0])  // Get Session Id
  //console.log(req.body.queryResult.intent)





  async function welcomeIntent(agent) {

    console.log('Entered welcomeHandler')
    //console.log(agent)

    const userProfileContext = agent.getContext('user_profile');
    if (userProfileContext == undefined) {
      agent.addResponse_(`Hi! I'm Friday!`)

      agent.addResponse_(` I'm here to assist and help you in choosing your future!`)

      agent.addResponse_(`What may I call you?`)
    }
    else {
      console.log('asdsadsadasd')
      agent.addResponse_(`Hi ${userProfileContext.parameters.name}. What can I do for you?`)
    }
  }

  async function comparisonNoHandler(agent) {
    agent.addResponse_(`Happy to assist you. Which one do you feel suits you best?
`)
    agent.addResponse_(new Suggestion("Data science"))
    agent.addResponse_(new Suggestion("Data Analytics"))

  }

  async function comparisonNobetterHandler(agent) {
    //agent.addResponse_(`Happy to assist you. Which one do you feel suits you best?`)
    agent.addResponse_("Excellent choice")

    agent.addResponse_("Data Science will be the most sought after profession in the next twenty years!")


    agent.addResponse_("Would you like to know about the financial aids for this course?")

  }

  // async function formtestHandler(agent) {
  //   agent.addResponse_('Thank You 12133 !')

  // }


  async function ticketHandler(agent) {
    agent.addResponse_('You can track your ticket closure status at this Link: http://helpcenter.com/')
    agent.addResponse_('You will also receive an email notification of the same.')
  }



  async function ThanksHandler(agent) {
    agent.addResponse_('Thank You !')
    agent.addResponse_(' I am right here if you need any assistance at all')
  }

  async function exitHandler(agent) {
    agent.addResponse_('Thank You !')
    agent.addResponse_(' I am right here if you need any assistance at all')
  }







  async function comparisonHandler(agent) {
    agent.addResponse_('From my expertise:\
Data science focuses on finding actionable insights from large sets of raw and structured data -\
to find answers to the things we don’t know.')

    agent.addResponse_('Data analytics focuses on processing and performing statistical analysis on existing datacreating methods to capture, process, and organize data to find \
actionable insights for current problems, and establishing the best way to present this data.')

    agent.addResponse_('Do you have more questions?')
  }

  async function emailHandleryes(agent) {
    agent.addResponse_('Data Science is one among the highest paid jobs in US for the last three years.\
The pay scale starts from 120K\
The university provides campus placements and opportunities for internships')


    agent.addResponse_('Can I help you with something else?')
  }

  async function yesSubscribeHandler(agent) {
    console.log("ebtered subscribe")
    agent.addResponse_('Data Science is one among the highest paid jobs in US for the last three years.\
The pay scale starts from 120K\
The university provides campus placements and opportunities for internships')


    agent.addResponse_('Can I help you with something else?')
  }


  async function courseRegisterHandler(agent) {

    agent.addResponse_(new Card({
      title: 'Course Register',
      imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRLXieJfkxFr8ivXDzBod_pcmUcVAJpgaoWHnRCh6W-fSqhIsSu',
      text: 'Register Now',
      buttonText: 'Register',
      buttonUrl: 'https://www.daad.de/deutschland/stipendium/en/'
    }));

  }
  async function comparisonNobetterHandleryes(agent) {
    agent.addResponse_('We provide scholarships for all students with GPA of 3 and above')
    agent.addResponse_('Do you need financial aid?')
  }
  async function finalendHandler(agent) {
    try {
      const userDetailsContext = agent.getContext('user_profile');
      const name = userDetailsContext.parameters.name
      agent.addResponse_('Thank you !' + name + 'I am right here if you need any assistance at all.')
    } catch (error) {
      agent.addResponse_('Thank you ! I am right here if you need any assistance at all.')
      console.log(error)
    }
    // const userDetailsContext = agent.getContext('user_profile');
    // const name = userDetailsContext.parameters.name
    // agent.addResponse_('Thank you !' + name + 'I am right here if you need any assistance at all.')
  }

  async function nohelpHandler(agent) {
    const userDetailsContext = agent.getContext('user_profile');
    const name = userDetailsContext.parameters.name
    agent.addResponse_('Thank you ! ' + name)
    agent.addResponse_('I am right here if you need any assistance at all')
  }




  async function subscribeHandler(agent) {
    const subemail = agent.parameters['email'];

    //const name = userDetailsContext.parameters.name
    try {
      //console.log('Entered personal message')
      let resp = await axios.get(urls.personalGreetingApi + '/personalgreetingmessage?email=' + subemail + '&name=' + '')
      //console.log(resp.data)
      //console.log(resp.data.result)
      const coursecontext = { 'name': 'user_course', 'lifespan': 100, 'parameters': { 'course': resp.data.course, 'emailid': subemail } };
      agent.setContext(coursecontext)
      //console.log(coursecontext + 'SDSADSADSADA')
      if (!resp.data.firstuser) {
        agent.addResponse_(resp.data.result)
      }
      // agent.addResponse_(new Suggestion('Yes'));
      //agent.addResponse_(new Suggestion('No'));
    } catch (err) {
      console.log("error" + err.message)
    }
    try {
      console.log('Entered create account')
      let res = await axios.get(urls.pipleData+'/'+ subemail)
      console.log(res.data)
    }
    catch (err) {
      console.log(err)
    }
  }
  async function financeHandler(agent) {
    agent.addResponse_(new Card({
      title: 'Financial Aid',
      imageUrl: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTgeyqyFs7YWRKKHVxvLAv4Bv_U_SYALvmEwTSvGBSdl5LVk86d',
      text: 'We look forward to help you solve all your financial issues',
      buttonText: 'Visit',
      buttonUrl: 'https://www.daad.de/deutschland/stipendium/en/'
    }));
    agent.addResponse_('Can I assist you with anything else ?')
  }

  async function enrollHandler(agent) {
    agent.addResponse_('Excellent!')
    agent.addResponse_('Would you like me to enroll you to this course now?')
    const usercourseContext = agent.getContext('user_course');
    const coursename = usercourseContext.parameters.course
    //console.log(coursename)
    agent.addResponse_(coursename)

  }

  async function yesenrollHandler(agent) {
    agent.addResponse_('Perfect!')
    agent.addResponse_('Could you please confirm your date of birth?')

  }

  async function dobenrollHandler(agent) {
    agent.addResponse_('Thank You !')
    agent.addResponse_('Could you please mention your preferred gender?')

  }




  async function genderenrollHandler(agent) {
    agent.addResponse_('Thank You !')
    agent.addResponse_('Your address please?')

  }
  async function addressenrollHandler(agent) {
    //console.log('addressssssssssss')
    agent.addResponse_('Is this address the same for communication?')

  }

  async function addressconfirmenrollHandler(agent) {

    agent.addResponse_('Noted')
    agent.addResponse_("Your parent or Guardian's name?")
  }

  async function financenoenrollHandler(agent) {

    agent.addResponse_('Great!')
    agent.addResponse_("Please complete the payment here to complete the enrolling process.")
    agent.add(new Suggestion('Make Payment'))
  }

  async function paymentenrollHandler(agent) {
    const usercourseContext = agent.getContext('user_course');
    const coursename = usercourseContext.parameters.course
    const emailids = usercourseContext.parameters.emailid
    agent.add("Voila! I'm pleased to inform you that you are now successfully enrolled for the course " + coursename + " ")
    agent.add(" Your Student ID is ID12345")
    agent.add(" Your login credentials are sent to your email ID " + emailids + " ")
    agent.add("You can also login to the LMS by visiting https://lms/ ")
  }

  async function fallbackIntent(agent) {
    console.log("queeeryy", agent.query)
    try {
      console.log('Entered generative ')
      const result = await axios.post(urls.generativeChatUrl, {
        "input": agent.query
      })
      // console.log(result)
      agent.addResponse_(result.data)

    }
    catch (err) {
      console.log(err)
    }
    //agent.addResponse_('Sorry didnt get you.')
  }

  async function addressnoenrollHandler(agent) {
    agent.setFollowupEvent('getAddressEvent')
  }





















  async function courseDetailHandler(agent) {
    const course = agent.parameters['course_name'];
    if (course == 'engineering') {
      agent.add(`Available courses in  ${course} are : .`);
      // agent.add(new Suggestion('Computer Science Engineering XI'))
      // agent.add(new Suggestion('Mechanical Engineering -Machine Drawing'))
      // agent.add(new Suggestion('Digital Signal Processing'))
      agent.add("Computer Science Engineering XI")
      agent.add("Mechanical Engineering -Machine Drawing")
      agent.add(" Digital Signal Processing")

    } else if (course == 'data') {
      agent.add('Data Science will be the most sought after profession in the next twenty years!');
      agent.addResponse_(" Data Science Basics")
      agent.addResponse_(" Data Analytics")
    }
    else if (course == 'Business and Management') {
      agent.add('Business and Management will be the most sought after profession in the next twenty years');
      agent.addResponse_("Account Manager")
      agent.addResponse_("Financial Analyst")
    } else {
      agent.add('Let me know your area of intrest');
    }


  }
  async function NameIntent(agent) {
    const name = agent.parameters['name'];

    const userInfo = (agent.getContext('user_course'));
    console.log(userInfo)
    if (userInfo !== null) {
      try {
        const email = userInfo.parameters.email
        console.log(userInfo.parameters.email, "userinfo")
        console.log('Entered personal message')
        let resp = await axios.get(urls.personalGreetingApi + '/personalgreetingmessage?email=' + email + '&name=' + name)
        //console.log(resp.data.course)
        const coursecontext = { 'name': 'user_course', 'lifespan': 100, 'parameters': { 'course': resp.data.course } };
        agent.setContext(coursecontext)
        const context = { 'name': 'user_profile', 'lifespan': 100, 'parameters': { 'name': name, 'email': email } };
        agent.setContext(context)
        agent.addResponse_(resp.data.result)
      } catch (err) {
        console.log(err.message)
      }
    }
    else {
      agent.addResponse_('Please enter your email')
    }


  }



  async function emailHandler(agent) {
    //console.log('Entered email handler')
    // const name = agent.parameters['given-name'];
    const email = agent.parameters['email'];
    const userDetailsContext = agent.getContext('awaiting_name');

    const name = userDetailsContext.parameters.name
    const context = { 'name': 'user_profile', 'lifespan': 100, 'parameters': { 'name': name, 'email': email } };
    agent.setContext(context)

    console.log(name, email)
    try {
      console.log('Entered create account')
      let res = await axios.get(urls.pipleData+'/'+ email)
      console.log(res.data)
    }
    catch (err) {
      console.log(err)
    }

    agent.addResponse_('Please choose one !')
    agent.add(new Suggestion('My to-do list'))
    agent.add(new Suggestion('Schedule'))
    agent.add(new Suggestion('Program related'))
    agent.add(new Suggestion('Support'))
    try {
      console.log('Entered create account')
      let res = await axios.post(urls.createLead, {
        email,
        name
      })
      console.log(res.data)
    }
    catch (err) {
      console.log(err)
    }
    // try {
    //   console.log('Entered create account')
    //   let res = await axios.get(urls.pipleData+'/'+email)
    //   console.log(res.data)
    // }
    // catch (err) {
    //   console.log(err)
    // }





    try {
      console.log('Entered personal message')
      let resp = await axios.get(urls.personalGreetingApi + '/personalgreetingmessage?email=' + email + '&name=' + name)
      //console.log(resp.data.course)
      const coursecontext = { 'name': 'user_course', 'lifespan': 100, 'parameters': { 'course': resp.data.course } };
      agent.setContext(coursecontext)
      agent.addResponse_(resp.data.result)
     
      // agent.addResponse_(new Suggestion('Yes'));
      //agent.addResponse_(new Suggestion('No'));



      //enrollHandler(agent,course=resp.data.course)
      

    } catch (err) {
      console.log(err.message)
    }
  }

    async function studentIntent(agent) {
      const name = agent.parameters['name'];

      const userInfo = (agent.getContext('user_course'));
      console.log(userInfo)
      if (userInfo !== null) {
        try {
          const email = userInfo.parameters.email
          console.log(userInfo.parameters.email, "userinfo")
          console.log('Entered personal message')
          let resp = await axios.get(urls.personalGreetingApi + '/personalgreetingmessage?email=' + email + '&name=' + name)
          //console.log(resp.data.course)
          const coursecontext = { 'name': 'user_course', 'lifespan': 100, 'parameters': { 'course': resp.data.course } };
          agent.setContext(coursecontext)
          const context = { 'name': 'user_profile', 'lifespan': 100, 'parameters': { 'name': name, 'email': email } };
          agent.setContext(context)
          agent.addResponse_(resp.data.result)
        } catch (err) {
          console.log(err.message)
        }
      }
      else {
        agent.addResponse_('Please enter your email')
      }
    }

  


  async function studentHandler(agent) {
    // console.log(agent)
    const studentContext = { 'name': 'is_student', 'lifespan': 100, 'parameters': { 'name': "name" } };
    agent.setContext(studentContext)
    console.log("hello==", agent.getContext("is_student"))
    agent.addResponse_('Please choose one !')
    agent.add(new Suggestion('My to-do list'))
    agent.add(new Suggestion('Schedule'))
    agent.add(new Suggestion('Program related'))
    agent.add(new Suggestion('Support'))

    
  }

  async function ProgramRelatedHandler(agent) {
    // console.log(agent)
    //const studentContext = { 'name': 'is_student', 'lifespan': 100, 'parameters': { 'name': "name" } };
    //agent.setContext(studentContext)
    //console.log("hello==", agent.getContext("is_student"))
    agent.addResponse_('Please choose one !')
    agent.add(new Suggestion('View My Courses '))
    agent.add(new Suggestion('Suggested Study Materials '))
    agent.add(new Suggestion('Alternate materials'))
    agent.add(new Suggestion('Course enrollment'))

    
  }
  async function myCoursesHandler(agent) {
    const userCourseContext = agent.getContext('user_profile');
    const mailid =userCourseContext.parameters.email;
    console.log("checkkkkkkkkkkk"+ mailid)
    
      try {
        console.log('Entered mycourse')
        const result = await axios.post(urls.getmycourseData, {
          "mailid": mailid
        })
        // console.log(result)
        console.log(result.data.map(x=>x.subject_name))
        for(let i of result.data) {
          agent.addResponse_(i.subject_name)
        }
       
        // result.data.map(x=>agent.addResponse_(x.subject_name))//.join("\n\n\n\n\n\n\n\n\n\n"))
  
      }
      catch (err) {
        console.log(err)
      }
      //agent.addResponse_('Sorry didnt get you.')
    }



async function allCourseHandler(agent) {
    // console.log(agent)
    const studentInfo = agent.getContext('is_student');
    //console.log(studentInfo)
    subjects = []
    if (studentInfo !== null) {
      agent.addResponse_('all courses')
      try {
        console.log("entereddddd")
        let resp = await axios.post(urls.getAllCoursesData)
        const courses = resp.data
        console.log(courses)
      //  courses.forEach(e=>{
          // console.log(courses.map(e=>e.section.map(x=>x.subject)))
          
          subjects.push(courses.map(e=>e.section.map(x=>x.subject)))
        // })
        console.log(subjects)
        let unique = [...new Set(subjects)];
        console.log(unique); 
        unique.forEach(e=>{
          agent.add(new Suggestion(e))
        })

      } catch (err) {
        //console.log(err.message)
      }

    }
    else {
      agent.addResponse_('Please let me know your area of interest.')
      // for (var subject in courses) {
      //   if( courses.hasOwnProperty(subject) ) {
      //   console.log(courses[subject])
      //   }
      // }
      // const result = await axios.post(urls.generativeChatUrl, {
      //   "input" : agent.query
      // })
    }
  }



  let intentMap = new Map();
  //intentMap.set('Make Appointment', makeAppointment);
  intentMap.set('Get Name', NameIntent);
  intentMap.set('Default Welcome Intent', welcomeIntent);
  intentMap.set('Default Fallback Intent', fallbackIntent);
  intentMap.set('get Course Details', courseDetailHandler)
  intentMap.set('get Email', emailHandler);
  intentMap.set('get Email - yes', emailHandleryes);
  intentMap.set('comparison Intent', comparisonHandler);
  intentMap.set('comparison Intent - no', comparisonNoHandler);
  intentMap.set('course Register', courseRegisterHandler);
  intentMap.set('comparison Intent - no - better', comparisonNobetterHandler);
  intentMap.set('comparison Intent - no - better - yes', comparisonNobetterHandleryes);
  intentMap.set('financial aid - yes', financeHandler);
  intentMap.set('  comparison Intent - no - better - yes - no - no - no  ', finalendHandler);
  intentMap.set('financial aid - no - no', nohelpHandler);
  intentMap.set('subscribed Email', subscribeHandler);
  intentMap.set('comparison Intent - no - better - no - no', ThanksHandler);
  intentMap.set('exit - no', exitHandler);
  intentMap.set('subscribed Email - yes', yesSubscribeHandler);
  intentMap.set('enroll Course', enrollHandler);
  intentMap.set('enroll Course - yes', yesenrollHandler);
  intentMap.set('enroll Course - dob', dobenrollHandler);
  intentMap.set('enroll Course - gender', genderenrollHandler);
  intentMap.set('enroll Course - address', addressenrollHandler);
  intentMap.set('enroll Course - address - yes', addressconfirmenrollHandler);
  intentMap.set('enroll Course - address - yes - parent - contact - finance-no', financenoenrollHandler);
  intentMap.set('make payment', paymentenrollHandler);
  intentMap.set('enroll Course - address - no', addressnoenrollHandler);
  //intentMap.set('form test', formtestHandler);
  intentMap.set('Ticket closed', ticketHandler);
  intentMap.set('student Intent', studentHandler);
  intentMap.set('all Course', allCourseHandler);
  intentMap.set('Program Related',ProgramRelatedHandler);
  intentMap.set('view my courses',myCoursesHandler);























  agent.handleRequest(intentMap);
});

app.listen(process.env.PORT || 8080);
